import { DI } from "./di"
import { Router } from "./router"
import { Timeout } from "./timeout"
import { HttpSource } from "./sources/http"
import { APNProvider } from "./providers/apn"
import { FCMProvider } from "./providers/fcm"
import { SmsTrafficProvider } from "./providers/smstraffic"
import { DevinoProvider } from "./providers/devino"
import { NeverProvider } from "./providers/never"
import { FailProvider } from "./providers/fail"
import { LogProvider } from "./providers/log"
import { HttpDelivered } from "./delivered/http"
import { Config } from "./services/Config"

export const Pushta = async (di: DI) => {
  const config = di(Config)

  if (config.router.enabled) await Router(di)
  if (config.timeout.enabled) await Timeout(di)
  if (config.sources.http.enabled) await HttpSource(di)
  if (config.providers.apn.enabled) await APNProvider(di)
  if (config.providers.fcm.enabled) await FCMProvider(di)
  if (config.providers.smstraffic.enabled) await SmsTrafficProvider(di)
  if (config.providers.devino.enabled) await DevinoProvider(di)
  if (config.providers.never.enabled) await NeverProvider(di)
  if (config.providers.fail.enabled) await FailProvider(di)
  if (config.providers.log.enabled) await LogProvider(di)
  if (config.delivered.http.enabled) await HttpDelivered(di)
}

if (require.main === module) {
  Pushta(DI())
}
