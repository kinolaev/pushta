import amqp from "amqplib"
import { sleep } from "../../utils"

export type Channel = Pick<
  amqp.Channel,
  | "assertQueue"
  | "bindQueue"
  | "assertExchange"
  | "publish"
  | "sendToQueue"
  | "consume"
  | "ack"
  | "reject"
>

interface AMPQCmd<T> {
  (ch: amqp.Channel): T
}

export const createChannel = async (url: string): Promise<Channel> => {
  let ch: null | amqp.Channel = null

  const init: Array<AMPQCmd<any>> = []
  const initCmd = <T>(cmd: AMPQCmd<T>): T => {
    if (ch === null) {
      throw new Error("NoChannel")
    }
    init.push(cmd)
    return cmd(ch)
  }

  const buffered: Array<AMPQCmd<any>> = []
  const bufferedCmd = <T>(cmd: AMPQCmd<T>, value: T): T => {
    if (ch === null) {
      buffered.push(cmd)
      return value
    }
    return cmd(ch)
  }

  const connect = (): PromiseLike<amqp.Connection> =>
    amqp.connect(url).catch((err) => {
      console.error(err.message)
      return sleep(1000).then(connect)
    })

  const createChannel = async (): Promise<void> => {
    ch = null

    const conn = await connect()
    conn.on("close", createChannel)

    const channel = await conn.createChannel()

    await Promise.all(init.map((cmd) => cmd(channel)))
    await Promise.all(buffered.splice(0).map((cmd) => cmd(channel)))

    ch = channel
  }

  await createChannel()

  return {
    assertQueue(queue, options) {
      return initCmd((ch) => ch.assertQueue(queue, options))
    },
    bindQueue(queue, source, pattern, args) {
      return initCmd((ch) => ch.bindQueue(queue, source, pattern, args))
    },
    assertExchange(exchange, type, options) {
      return initCmd((ch) => ch.assertExchange(exchange, type, options))
    },
    publish(exchange, routingKey, content, options) {
      return bufferedCmd(
        (ch) => ch.publish(exchange, routingKey, content, options),
        true
      )
    },
    sendToQueue(queue, content, options) {
      return bufferedCmd((ch) => ch.sendToQueue(queue, content, options), true)
    },
    consume(queue, onMessage, options) {
      return initCmd((ch) => ch.consume(queue, onMessage, options))
    },
    ack(message, allUpTo) {
      return ch === null ? undefined : ch.ack(message, allUpTo)
    },
    reject(message, requeue) {
      return ch === null ? undefined : ch.reject(message, requeue)
    },
  }
}

export interface Decode<T> {
  (msg: amqp.ConsumeMessage): Error | T
}
export interface Consume<T> {
  (f: (msg: T) => Promise<void>): PromiseLike<amqp.Replies.Consume>
}

export const createConsume =
  <T>(
    ch: Channel,
    queue: string,
    decodeMessage: (msg: amqp.ConsumeMessage) => Error | T,
    options?: amqp.Options.Consume
  ): Consume<T> =>
  (handleMessage) =>
    ch.consume(
      queue,
      (msg) => {
        if (msg === null) return

        const decoded = decodeMessage(msg)
        if (decoded instanceof Error) {
          return ch.reject(msg, false)
        }

        return handleMessage(decoded)
          .then(() => ch.ack(msg))
          .catch((error) => {
            console.error(error)
            setTimeout(() => {
              ch.reject(msg, true)
            }, 1000)
          })
      },
      options
    )
