import type { DI } from "../di"
import type { Store as IStore } from "./Store/Store"
import { KnexStore } from "./Store/KnexStore"
import { MemoryStore } from "./Store/MemoryStore"
import { Config } from "./Config"

export const Store = (di: DI): IStore => {
  const config = di(Config)
  if (
    config.databaseUrl.startsWith("postgres://") ||
    config.databaseUrl.startsWith("postgresql://")
  ) {
    return new KnexStore(config.databaseUrl)
  }
  return new MemoryStore()
}
