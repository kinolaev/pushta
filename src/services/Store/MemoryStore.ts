import type {
  MessageInput,
  TaskInput,
  Message,
  Task,
  Target,
  ProviderDelivered,
} from "../../data"
import type { Store } from "./Store"

export class MemoryStore implements Store {
  private id = 0
  private messages: Array<Message> = []
  private tasks: Array<Task> = []
  private targets: Array<Target> = []
  private providerDelivered: Array<ProviderDelivered> = []

  async addMessage(msg: MessageInput): Promise<string> {
    const message_id = this.getId()

    this.messages.push({
      id: message_id,
      created_at: Date.now(),
      text: msg.text,
      ttl: msg.ttl ?? null,
      data: Array.isArray(msg.options) ? msg.options : null,
      options: Array.isArray(msg.options) ? msg.options : null,
    })

    msg.tasks.forEach((task, i) =>
      this.addTasksAndTargets(message_id, [], i, task)
    )

    return message_id
  }

  async getMessage(id: string): Promise<Message | undefined> {
    return this.messages.find((m) => m.id === id)
  }

  async getTask(message_id: string, id: string): Promise<Task | undefined> {
    return this.tasks.find(
      (task) => task.message_id === message_id && task.id === id
    )
  }

  async getTasks(
    messageId: string,
    parentId: null | string,
    maxDepth: null | number
  ): Promise<Array<Task>> {
    const prefix = parentId === null ? null : parentId + "."
    return this.tasks.filter(
      (task) =>
        task.message_id === messageId &&
        (prefix === null || task.id.startsWith(prefix)) &&
        (maxDepth === null || task.depth <= maxDepth)
    )
  }

  async setTaskStartedAt(task: Task, startedAt: number): Promise<void> {
    task.started_at = startedAt
  }

  async setTaskFailedAt(task: Task, failedAt: number): Promise<void> {
    task.failed_at = failedAt
  }

  async setTaskSucceedAt(task: Task, succeedAt: number): Promise<void> {
    task.succeed_at = succeedAt
  }

  async getTargets(
    messageId: string,
    taskId: null | string
  ): Promise<Array<Target>> {
    return this.targets.filter(
      (target) =>
        target.message_id === messageId &&
        (taskId === null || target.task_id === taskId)
    )
  }

  async setTargetError(target: Target, error: string): Promise<void> {
    target.error = error
  }

  async setTargetExtId(target: Target, extId: string): Promise<void> {
    target.ext_id = extId
  }

  async setTargetDeliveredAt(
    target: Target,
    deliveredAt: number
  ): Promise<void> {
    target.delivered_at = deliveredAt
  }

  async getTarget(
    messageId: string,
    taskId: string,
    id: string
  ): Promise<Target | undefined> {
    return this.targets.find(
      (target) =>
        target.message_id === messageId &&
        target.task_id === taskId &&
        target.id === id
    )
  }

  async getTargetByExtId(
    provider: string,
    extId: string
  ): Promise<Array<Target>> {
    return this.targets.filter(
      (target) => target.provider === provider && target.ext_id === extId
    )
  }

  async addProviderDelivered(delivered: ProviderDelivered): Promise<void> {
    this.providerDelivered.push(delivered)
  }

  async getProviderDelivered(
    provider: string,
    id: string
  ): Promise<ProviderDelivered | undefined> {
    return this.providerDelivered.find(
      (d) => d.provider === provider && d.id === id
    )
  }

  async removeProviderDelivered(provider: string, id: string): Promise<void> {
    this.providerDelivered = this.providerDelivered.filter(
      (d) => d.provider !== provider || d.id !== id
    )
  }

  private getId(): string {
    return (++this.id).toString()
  }

  private addTasksAndTargets(
    message_id: string,
    prefix: Array<number>,
    i: number,
    task: TaskInput
  ) {
    const path = prefix.concat(i)
    const taskId = path.join(".")
    this.tasks.push({
      message_id,
      id: taskId,
      started_at: null,
      failed_at: null,
      succeed_at: null,
      depth: path.length,
      ttl: task.ttl ?? null,
      options: task.options ?? [],
    })
    for (let i = 0, l = task.targets.length; i < l; i++) {
      const target = task.targets[i]
      this.targets.push({
        message_id,
        task_id: taskId,
        id: i.toString(),
        provider: target.provider,
        address: target.address,
        ttl: target.ttl ?? null,
        data: target.data,
        options: target.options,
        ext_id: null,
        error: null,
        delivered_at: null,
      })
    }
    if (Array.isArray(task.fallback)) {
      task.fallback.forEach((task, i) =>
        this.addTasksAndTargets(message_id, path, i, task)
      )
    }
  }
}
