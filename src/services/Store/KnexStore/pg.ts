import type { Knex, ResolveResult } from "knex"
import type { Pool, PoolClient } from "pg"
import map from "lodash/map"

export type QueryBuilderQuery = <TRecord, TResult>(
  qb: Knex.QueryBuilder<TRecord, TResult>
) => Promise<ResolveResult<TResult>>

export async function query<TRecord, TResult>(
  client: Pool | PoolClient,
  qb: Knex.QueryBuilder<TRecord, TResult>
): Promise<ResolveResult<TResult>> {
  const obj = qb.toSQL()
  const native = obj.toNative()
  return client
    .query(native.sql, (native.bindings as Array<any>) || [])
    .then((resp) => {
      // https://github.com/knex/knex/blob/0.95.8/lib/dialects/postgres/index.js#L210
      if (obj.method === "raw") return resp
      // @ts-ignore
      const { returning } = obj
      if (resp.command === "SELECT") {
        if (obj.method === "first") return resp.rows[0]
        // @ts-ignore
        if (obj.method === "pluck") return map(resp.rows, obj.pluck)
        return resp.rows
      }
      if (returning) {
        const returns = []
        for (let i = 0, l = resp.rows.length; i < l; i++) {
          const row = resp.rows[i]
          if (returning === "*" || Array.isArray(returning)) {
            returns[i] = row
          } else {
            // Pluck the only column in the row.
            returns[i] = row[Object.keys(row)[0]]
          }
        }
        return returns
      }
      if (resp.command === "UPDATE" || resp.command === "DELETE") {
        return resp.rowCount
      }
      return resp
    })
}

export type TransactionExecutor<R> = (query: QueryBuilderQuery) => Promise<R>

export async function transaction<R>(
  pool: Pool,
  executor: TransactionExecutor<R>
): Promise<R> {
  const client = await pool.connect()
  try {
    await client.query("BEGIN")
    const result = await executor((qb) => query(client, qb))
    await client.query("COMMIT")
    return result
  } catch (e) {
    await client.query("ROLLBACK")
    throw e
  } finally {
    client.release()
  }
}
