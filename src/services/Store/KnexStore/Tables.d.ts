import { Knex } from "knex"
import { Message, Task, Target, ProviderDelivered } from "../../../data"

declare module "knex/types/tables" {
  interface Tables {
    messages: Message
    messages_composite: Knex.CompositeTableType<
      Message,
      Omit<Message, "id" | "created_at">,
      Partial<Omit<Message, "id" | "created_at">>
    >

    tasks: Task
    tasks_composite: Knex.CompositeTableType<Task, Task, Partial<Task>>

    targets: Target
    targets_composite: Knex.CompositeTableType<Target, Target, Partial<Target>>

    delivered: ProviderDelivered
  }
}
