import { Knex } from "knex"

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable("messages", (table) => {
      table.bigIncrements("id")
      table.timestamp("created_at").notNullable().defaultTo(knex.fn.now())
      table.text("text").notNullable()
      table.integer("ttl").unsigned()
      table.json("data")
      table.json("options")
    })
    .createTable("messages_tags", (table) => {
      table.bigInteger("message_id").unsigned().notNullable()
      table.text("tag").notNullable()

      table
        .foreign("message_id")
        .references("id")
        .inTable("messages")
        .onDelete("cascade")
      table.unique(["message_id", "tag"])
      table.index("tag")
    })
    .createTable("tasks", (table) => {
      table.bigInteger("message_id").unsigned().notNullable()
      table.text("id").notNullable()
      table.integer("depth").notNullable()
      table.integer("ttl").unsigned()
      table.timestamp("started_at")
      table.timestamp("failed_at")
      table.timestamp("succeed_at")
      table.json("options")

      table.primary(["message_id", "id"])
      table
        .foreign("message_id")
        .references("id")
        .inTable("messages")
        .onDelete("cascade")
    })
    .createTable("targets", (table) => {
      table.bigInteger("message_id").unsigned().notNullable()
      table.text("task_id").notNullable()
      table.integer("id").unsigned().notNullable()
      table.text("provider").notNullable()
      table.text("address").notNullable()
      table.integer("ttl").unsigned()
      table.json("data")
      table.json("options")
      table.text("ext_id")
      table.text("error")
      table.timestamp("delivered_at")

      table.primary(["message_id", "task_id", "id"])
      table
        .foreign("message_id")
        .references("id")
        .inTable("messages")
        .onDelete("cascade")
    })
    .createTable("delivered", (table) => {
      table.text("provider")
      table.text("id")
      table.timestamp("created_at").notNullable().defaultTo(knex.fn.now())

      table.primary(["provider", "id"])
    })
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .dropTable("delivered")
    .dropTable("targets")
    .dropTable("tasks")
    .dropTable("messages_tags")
    .dropTable("messages")
}
