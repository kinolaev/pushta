import type {
  MessageInput,
  Message,
  Task,
  Target,
  ProviderDelivered,
} from "../../data"

export interface Store {
  addMessage(msg: MessageInput): Promise<string>
  getMessage(messageId: string): Promise<Message | undefined>

  getTask(messageId: string, taskId: string): Promise<Task | undefined>
  getTasks(
    messageId: string,
    parentId: string | null,
    maxDepth: number | null
  ): Promise<Array<Task>>
  setTaskStartedAt(task: Task, startedAt: number): Promise<void>
  setTaskFailedAt(task: Task, failedAt: number): Promise<void>
  setTaskSucceedAt(task: Task, succeedAt: number): Promise<void>

  getTargets(messageId: string, taskId: null | string): Promise<Array<Target>>
  getTarget(
    messageId: string,
    taskId: string,
    id: string
  ): Promise<Target | undefined>
  getTargetByExtId(provider: string, extId: string): Promise<Array<Target>>
  setTargetError(target: Target, error: string): Promise<void>
  setTargetExtId(target: Target, extId: string): Promise<void>
  setTargetDeliveredAt(target: Target, deliveredAt: number): Promise<void>

  addProviderDelivered(delivered: ProviderDelivered): Promise<void>
  getProviderDelivered(
    provider: string,
    id: string
  ): Promise<ProviderDelivered | undefined>
  removeProviderDelivered(provider: string, id: string): Promise<void>
}
