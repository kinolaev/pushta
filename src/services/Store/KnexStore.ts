import { knex, Knex } from "knex"
import { Pool } from "pg"
import {
  Message,
  Task,
  Target,
  MessageInput,
  TaskInput,
  ProviderDelivered,
} from "../../data"
import type { Store } from "./Store"
import {
  query,
  transaction,
  QueryBuilderQuery,
  TransactionExecutor,
} from "./KnexStore/pg"

export class KnexStore implements Store {
  private pool: Pool
  private knex: Knex

  constructor(connectionString: string) {
    this.pool = new Pool({ connectionString })
    this.knex = knex({ client: "pg" })
  }

  addMessage(msg: MessageInput): Promise<string> {
    return this.transaction(async (query) => {
      const messageQb = this.knex("messages")
        .insert({
          text: msg.text,
          ttl: msg.ttl ?? null,
          // @ts-ignore
          data: Array.isArray(msg.data) ? JSON.stringify(msg.data) : null,
          // @ts-ignore
          options: Array.isArray(msg.options)
            ? JSON.stringify(msg.options)
            : null,
        })
        .returning("id")
      const rows = await query(messageQb)

      const id = rows[0]
      const tasks: Array<Task> = []
      const targets: Array<Target> = []

      if (Array.isArray(msg.tags)) {
        const tags = msg.tags.map((tag) => ({ message_id: id, tag }))
        await query(this.knex("messages_tags").insert(tags))
      }

      msg.tasks.forEach((task, i) =>
        addTasksAndTargets(tasks, targets, id, [], i, task)
      )
      await query(this.knex("tasks").insert(tasks))
      await query(this.knex("targets").insert(targets))

      return id
    })
  }

  getMessage(id: string): Promise<Message | undefined> {
    const qb = this.knex("messages").where("id", id).first()
    return this.query(qb)
  }

  getTask(messageId: string, id: string): Promise<Task | undefined> {
    const qb = this.knex("tasks")
      .first()
      .where("message_id", messageId)
      .andWhere("id", id)
    return this.query(qb)
  }

  getTasks(
    messageId: string,
    parentId: string | null,
    maxDepth: number | null
  ): Promise<Array<Task>> {
    const qb = this.knex("tasks").select().where("message_id", messageId)
    if (parentId !== null) {
      qb.andWhere("id", "like", parentId + ".%")
    }
    if (maxDepth !== null) {
      qb.andWhere("depth", "<=", maxDepth)
    }
    return this.query(qb)
  }

  async setTaskStartedAt(task: Task, startedAt: number): Promise<void> {
    const qb = this.knex("tasks")
      .update("started_at", new Date(startedAt))
      .where("message_id", task.message_id)
      .andWhere("id", task.id)
    await this.query(qb)
    task.started_at = startedAt
  }

  async setTaskFailedAt(task: Task, failedAt: number): Promise<void> {
    const qb = this.knex("tasks")
      .update("failed_at", new Date(failedAt))
      .where("message_id", task.message_id)
      .andWhere("id", task.id)
    await this.query(qb)
    task.failed_at = failedAt
  }

  async setTaskSucceedAt(task: Task, succeedAt: number): Promise<void> {
    const qb = this.knex("tasks")
      .update("succeed_at", new Date(succeedAt))
      .where("message_id", task.message_id)
      .andWhere("id", task.id)
    await this.query(qb)
    task.succeed_at = succeedAt
  }

  getTarget(
    messageId: string,
    taskId: string,
    id: string
  ): Promise<Target | undefined> {
    const qb = this.knex("targets")
      .first()
      .where("message_id", messageId)
      .andWhere("task_id", taskId)
      .andWhere("id", id)
    return this.query(qb)
  }

  getTargetByExtId(provider: string, extId: string): Promise<Array<Target>> {
    const qb = this.knex("targets")
      .select()
      .where("provider", provider)
      .andWhere("ext_id", extId)
    return this.query(qb)
  }

  getTargets(messageId: string, taskId: null | string): Promise<Array<Target>> {
    const qb = this.knex("targets").where("message_id", messageId)
    if (taskId !== null) {
      qb.andWhere("task_id", taskId)
    }
    return this.query(qb)
  }

  async setTargetError(target: Target, error: string): Promise<void> {
    const qb = this.knex("targets")
      .update("error", error)
      .where("message_id", target.message_id)
      .andWhere("task_id", target.task_id)
      .andWhere("id", target.id)
    await this.query(qb)
    target.error = error
  }

  async setTargetExtId(target: Target, extId: string): Promise<void> {
    const qb = this.knex("targets")
      .update("ext_id", extId)
      .where("message_id", target.message_id)
      .andWhere("task_id", target.task_id)
      .andWhere("id", target.id)
    await this.query(qb)
    target.ext_id = extId
  }

  async setTargetDeliveredAt(
    target: Target,
    deliveredAt: number
  ): Promise<void> {
    const qb = this.knex("targets")
      .update("delivered_at", new Date(deliveredAt))
      .where("message_id", target.message_id)
      .andWhere("task_id", target.task_id)
      .andWhere("id", target.id)
    await this.query(qb)
    target.delivered_at = deliveredAt
  }

  async addProviderDelivered(delivered: ProviderDelivered): Promise<void> {
    const qb = this.knex.into("delivered").insert(delivered)
    await this.query(qb)
  }

  getProviderDelivered(
    provider: string,
    id: string
  ): Promise<ProviderDelivered | undefined> {
    const qb = this.knex("delivered")
      .first()
      .where("provider", provider)
      .andWhere("id", id)
    return this.query(qb)
  }

  async removeProviderDelivered(provider: string, id: string): Promise<void> {
    const qb = this.knex("delivered")
      .delete()
      .where("provider", provider)
      .andWhere("id", id)
    await this.query(qb)
  }

  private query: QueryBuilderQuery = (qb) => query(this.pool, qb)

  private transaction<R>(executor: TransactionExecutor<R>): Promise<R> {
    return transaction(this.pool, executor)
  }
}

const addTasksAndTargets = (
  tasks: Array<Task>,
  targets: Array<Target>,
  message_id: string,
  prefix: Array<number>,
  i: number,
  task: TaskInput
): void => {
  const path = prefix.concat(i)
  const taskId = path.join(".")
  tasks.push({
    message_id,
    id: taskId,
    started_at: null,
    failed_at: null,
    succeed_at: null,
    depth: path.length,
    ttl: task.ttl ?? null,
    // @ts-ignore
    options: Array.isArray(task.options) ? JSON.stringify(task.options) : null,
  })
  for (let i = 0, l = task.targets.length; i < l; i++) {
    const target = task.targets[i]
    targets.push({
      message_id,
      task_id: taskId,
      id: i.toString(),
      provider: target.provider,
      address: target.address,
      ttl: target.ttl ?? null,
      data: target.data ?? null,
      options: target.options ?? null,
      ext_id: null,
      error: null,
      delivered_at: null,
    })
  }
  if (Array.isArray(task.fallback)) {
    task.fallback.forEach((task, i) =>
      addTasksAndTargets(tasks, targets, message_id, path, i, task)
    )
  }
}
