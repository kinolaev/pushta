import type { DI } from "../di"

const { env } = process

export const Config = (di: DI) => ({
  prefix: env.PUSHTA_PREFIX ?? "pushta",
  secret: env.PUSHTA_SECRET ?? "pushta",
  amqpUrl: env.PUSHTA_AMQP_URL ?? "amqp://localhost",
  databaseUrl: env.PUSHTA_DATABASE_URL ?? env.DATABASE_URL ?? "",
  router: {
    enabled: bool(env.PUSHTA_ROUTER_ENABLED, true),
  },
  timeout: {
    enabled: bool(env.PUSHTA_TIMEOUT_ENABLED, true),
  },
  sources: {
    http: {
      enabled: bool(env.PUSHTA_SOURCES_HTTP_ENABLED, true),
      port: num(env.PUSHTA_SOURCES_HTTP_PORT, 5000),
    },
  },
  providers: {
    apn: {
      enabled: bool(env.PUSHTA_PROVIDERS_APN_ENABLED, true),
      token: {
        key: env.PUSHTA_PROVIDERS_APN_TOKEN_KEY ?? "",
        keyId: env.PUSHTA_PROVIDERS_APN_TOKEN_KEY_ID ?? "",
        teamId: env.PUSHTA_PROVIDERS_APN_TOKEN_TEAM_ID ?? "",
      },
      production: bool(env.PUSHTA_PROVIDERS_APN_PRODUCTION, true),
      bundleId: env.PUSHTA_PROVIDERS_APN_BUNDLE_ID ?? "",
    },
    fcm: {
      enabled: bool(env.PUSHTA_PROVIDERS_FCM_ENABLED, true),
      credentials: env.PUSHTA_PROVIDERS_FCM_CREDENTIALS ?? "",
    },
    smstraffic: {
      enabled: bool(env.PUSHTA_PROVIDERS_SMSTRAFFIC_ENABLED, false),
      url:
        env.PUSHTA_PROVIDERS_SMSTRAFFIC_URL ??
        "https://api.smstraffic.ru/multi.php",
      login: env.PUSHTA_PROVIDERS_SMSTRAFFIC_LOGIN ?? "",
      password: env.PUSHTA_PROVIDERS_SMSTRAFFIC_PASSWORD ?? "",
      rus: env.PUSHTA_PROVIDERS_SMSTRAFFIC_RUS ?? "5",
    },
    devino: {
      enabled: bool(env.PUSHTA_PROVIDERS_DEVINO_ENABLED, false),
      url:
        env.PUSHTA_PROVIDERS_DEVINO_URL ??
        "https://xmlapi.devinotele.com/Send.ashx",
      login: env.PUSHTA_PROVIDERS_DEVINO_LOGIN ?? "",
      password: env.PUSHTA_PROVIDERS_DEVINO_PASSWORD ?? "",
      sender: env.PUSHTA_PROVIDERS_DEVINO_SENDER ?? "",
    },
    never: {
      enabled: bool(env.PUSHTA_PROVIDERS_NEVER_ENABLED, false),
    },
    fail: {
      enabled: bool(env.PUSHTA_PROVIDERS_FAIL_ENABLED, false),
    },
    log: {
      enabled: bool(env.PUSHTA_PROVIDERS_LOG_ENABLED, false),
    },
  },
  delivered: {
    http: {
      enabled: bool(env.PUSHTA_DELIVERED_HTTP_ENABLED, true),
      port: num(env.PUSHTA_DELIVERED_HTTP_PORT, 5001),
    },
  },
})

const num = (value: string | undefined, defaultValue: number = 0): number =>
  value === undefined ? defaultValue : +value

const bool = (
  value: string | undefined,
  defaultValue: boolean = false
): boolean =>
  value === undefined
    ? defaultValue
    : value !== "" && value !== "0" && value.toLowerCase() !== "false"
