import type { DI } from "../di"
import type {
  Message,
  Task,
  ProvidersData,
  ProviderMessage,
  ProviderTarget,
  Completed,
  Target,
} from "../data"
import { timestamp, minPositiveOrNull, jwtSignature } from "../utils"
import {
  ClearTimeoutExchange,
  CompletedExchange,
  ProviderQueue,
  SetTimeoutQueue,
} from "./AMQP"
import { Config } from "./Config"
import { Store } from "./Store"

export const TaskService = async (di: DI) => {
  const config = di(Config)
  const store = di(Store)
  const setTimeoutQueue = await di(SetTimeoutQueue)
  const clearTimeoutExchange = await di(ClearTimeoutExchange)
  const providerQueue = await di(ProviderQueue)
  const completedExchange = await di(CompletedExchange)

  const timeoutIdPrefix = config.prefix + ".target."
  const makeTimeoutId = (targetId: string): string => timeoutIdPrefix + targetId
  const parseTimeoutId = (timeoutId: string): [string, string, string] =>
    timeoutId.startsWith(timeoutIdPrefix)
      ? parseTargetId(timeoutId.substring(timeoutIdPrefix.length))
      : ["", "", ""]

  const startMessage = async (id: string): Promise<void> => {
    const message = await store.getMessage(id)
    if (message === undefined) return

    if (message.ttl !== null) {
      const timeoutId = makeTimeoutId(makeTargetId(message.id))
      const expire = message.created_at + message.ttl * 1000
      setTimeoutQueue.publish(timeoutId, expire - Date.now())
    }

    const tasks = await store.getTasks(message.id, null, 1)
    for (const task of tasks) {
      await startTask(message, task)
    }
  }

  const messageTimeout = async (id: string): Promise<void> => {
    const tasks = await store.getTasks(id, null, 1)
    const failureCount = tasks.filter((task) => task.failed_at !== null).length
    const successCount = tasks.filter((task) => task.succeed_at !== null).length

    if (tasks.length === failureCount + successCount) return

    sendCompleted({
      id,
      successCount,
      failureCount: tasks.length - successCount,
    })
  }

  const startTask = async (msg: Message, task: Task): Promise<void> => {
    if (isMessageExpired(msg)) return
    if (task.started_at !== null) return

    await store.setTaskStartedAt(task, Date.now())
    if (task.ttl !== null) {
      const targetId = makeTargetId(task.message_id, task.id)
      setTimeoutQueue.publish(makeTimeoutId(targetId), task.ttl * 1000)
    }

    const msgData = msg.data ?? []
    const msgOpts = msg.options ?? []
    const taskOpts = task.options ?? []
    const taskExp = getTaskExpire(msg, task)
    const targets = await store.getTargets(msg.id, task.id)
    const targetsByProviders: { [provider: string]: Array<ProviderTarget> } = {}
    for (const target of targets) {
      const targetId = getTargetId(target)
      const targetExp =
        target.ttl === null ? null : timestamp(task.started_at!) + target.ttl
      const providerTarget = {
        id: targetId,
        token: getTargetToken(target, config.secret),
        address: target.address,
        exp: minPositiveOrNull(taskExp, targetExp),
        data: target.data,
        options: target.options,
      }
      if (targetsByProviders.hasOwnProperty(target.provider)) {
        targetsByProviders[target.provider].push(providerTarget)
      } else {
        targetsByProviders[target.provider] = [providerTarget]
      }

      if (target.ttl !== null) {
        setTimeoutQueue.publish(makeTimeoutId(targetId), target.ttl * 1000)
      }
    }

    for (const [provider, targets] of Object.entries(targetsByProviders)) {
      const providerMessage: ProviderMessage = {
        text: msg.text,
        targets,
        data: getProviderData(provider, msgData),
        options: getProviderData(provider, msgOpts.concat(taskOpts)),
      }
      providerQueue.publish(provider, providerMessage)
    }
  }

  const taskFailed = (messageId: string, id: string): Promise<void> =>
    completeTask(messageId, id, false)

  const taskSucceed = (messageId: string, id: string): Promise<void> =>
    completeTask(messageId, id, true)

  const targetFailed = (
    messageId: string,
    taskId: string,
    id: string,
    error: string
  ): Promise<void> => completeTarget(messageId, taskId, id, error, null)

  const targetDelivered = (
    messageId: string,
    taskId: string,
    id: string,
    deliveredAt: number
  ): Promise<void> => completeTarget(messageId, taskId, id, null, deliveredAt)

  const completeTask = async (
    messageId: string,
    id: string,
    success: boolean
  ) => {
    const task = await store.getTask(messageId, id)
    if (task === undefined) return
    if (task.failed_at !== null || task.succeed_at !== null) return
    if (success) {
      await store.setTaskSucceedAt(task, Date.now())
      return taskCompleted(messageId, id)
    }
    const fallback = await store.getTasks(messageId, id, task.depth + 1)
    if (fallback.length === 0 || fallback.every((t) => t.failed_at !== null)) {
      await store.setTaskFailedAt(task, Date.now())
      return taskCompleted(messageId, id)
    }

    const msg = await store.getMessage(messageId)
    if (msg === undefined) return

    for (const task of fallback) {
      await startTask(msg, task)
    }
  }

  const taskCompleted = async (
    messageId: string,
    taskId: string
  ): Promise<void> => {
    clearTimeoutExchange.publish(
      makeTimeoutId(makeTargetId(messageId, taskId, "*"))
    )

    const depth = taskId.split(".").length

    if (depth > 1) {
      const parentId = taskId.substring(0, taskId.lastIndexOf("."))
      const children = await store.getTasks(messageId, parentId, depth)
      const minSuccessCount = 1
      const maxFailureCount = children.length

      const successCount = children.filter((t) => t.succeed_at !== null).length
      if (successCount >= minSuccessCount)
        return taskSucceed(messageId, parentId)

      const failureCount = children.filter((t) => t.failed_at !== null).length
      if (failureCount >= maxFailureCount)
        return taskFailed(messageId, parentId)
    } else {
      const message = await store.getMessage(messageId)
      if (message === undefined) return

      if (isMessageExpired(message)) return

      const children = await store.getTasks(messageId, null, 1)
      const failureCount = children.filter((t) => t.failed_at !== null).length
      const successCount = children.filter((t) => t.succeed_at !== null).length
      if (children.length > failureCount + successCount) return

      sendCompleted({ id: messageId, failureCount, successCount })
    }
  }

  const completeTarget = async (
    messageId: string,
    taskId: string,
    id: string,
    error: null | string,
    deliveredAt: null | number
  ): Promise<void> => {
    const target = await store.getTarget(messageId, taskId, id)
    if (target === undefined) return
    if (target.delivered_at !== null) return
    if (error === null) {
      await store.setTargetDeliveredAt(target, deliveredAt ?? Date.now())
      if (target.error === null) {
        return targetCompleted(messageId, taskId, id)
      }
    } else {
      if (target.error === null) {
        await store.setTargetError(target, error)
        return targetCompleted(messageId, taskId, id)
      }
    }
  }

  const targetCompleted = async (
    messageId: string,
    taskId: string,
    id: string
  ) => {
    clearTimeoutExchange.publish(
      makeTimeoutId(makeTargetId(messageId, taskId, id))
    )

    const targets = await store.getTargets(messageId, taskId)

    const failureCount = targets.filter((t) => t.error !== null).length
    const successCount = targets.filter((t) => t.delivered_at !== null).length

    const minSuccessCount = 1
    const maxFailureCount = targets.length
    if (successCount >= minSuccessCount) {
      return taskSucceed(messageId, taskId)
    }
    if (failureCount >= maxFailureCount) {
      return taskFailed(messageId, taskId)
    }
  }

  const sendCompleted = (completed: Completed): boolean => {
    const targetId = makeTargetId(completed.id, "", "*")
    clearTimeoutExchange.publish(makeTimeoutId(targetId))
    console.log("completed", completed)
    return completedExchange.publish(completed)
  }

  return {
    parseTimeoutId,
    startMessage,
    messageTimeout,
    startTask,
    taskFailed,
    targetFailed,
    targetDelivered,
  }
}

export const getProviderData = (
  provider: string,
  data: Array<ProvidersData>
): Array<unknown> =>
  data.filter(isProviderData(provider)).map((data) => data.data)

const isProviderData = (provider: string) => (data: ProvidersData) =>
  data.providers === null || data.providers.includes(provider)

const isMessageExpired = (msg: Message): boolean =>
  msg.ttl !== null && msg.created_at + msg.ttl * 1000 < Date.now()

const makeTargetId = (mId: string, tId: string = "", id: string = "") =>
  [mId, tId, id].join("-")
export const getTargetId = (target: Target): string =>
  makeTargetId(target.message_id, target.task_id, target.id)
export const parseTargetId = (targetId: string): [string, string, string] => {
  const [mId = "", tId = "", id = ""] = targetId.split("-", 3)
  return [mId, tId, id]
}

const getTaskExpire = (msg: Message, task: Task): null | number => {
  const msgExp = msg.ttl === null ? null : timestamp(msg.created_at) + msg.ttl
  const taskExp =
    task.started_at === null || task.ttl === null
      ? null
      : timestamp(task.started_at) + task.ttl
  return minPositiveOrNull(msgExp, taskExp)
}

export const getTargetToken = (target: Target, secret: string): string =>
  jwtSignature(
    JSON.stringify([
      target.message_id,
      target.task_id,
      target.id,
      target.provider,
      target.address,
    ]),
    secret
  )
