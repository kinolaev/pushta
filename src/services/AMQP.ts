import type amqp from "amqplib"
import type {
  Completed,
  Delivered,
  Failed,
  ProviderMessage,
  SentResult,
} from "../data"
import type { DI } from "../di"
import type { Channel as AMQPChannel, Consume, Decode } from "./AMPQ/utils"

import { jsonBuf, parseJsonBuf } from "../utils"
import { Config } from "./Config"
import { createChannel, createConsume } from "./AMPQ/utils"

export const Channel = async (di: DI): Promise<AMQPChannel> => {
  const config = di(Config)
  return createChannel(config.amqpUrl)
}

export const MessagesQueue = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const q = await ch.assertQueue(config.prefix + ".router.messages")

  const publish = (id: string): boolean =>
    ch.publish("", q.queue, Buffer.from(id, "utf8"), { persistent: true })

  const decode: Decode<string> = (msg) => msg.content.toString("utf8")

  return {
    publish,
    consume: createConsume(ch, q.queue, decode),
  }
}

export const SetTimeoutQueue = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const q = await ch.assertQueue(config.prefix + ".timeout.set")

  const publish = (id: string, ms: number): boolean => {
    const isoDate = new Date(Date.now() + ms).toISOString()
    return ch.publish("", q.queue, Buffer.from(isoDate, "utf8"), {
      correlationId: id,
      persistent: true,
    })
  }

  const decode = (msg: amqp.ConsumeMessage) =>
    typeof msg.properties.correlationId === "string" &&
    msg.properties.correlationId !== ""
      ? {
          id: msg.properties.correlationId,
          expire: msg.content.toString("utf8"),
        }
      : new Error("No correlationId")

  return {
    publish,
    consume: createConsume(ch, q.queue, decode),
  }
}

export const TimeoutQueue = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const q = await ch.assertQueue(config.prefix + ".router.timeout")
  const emptyBuffer = Buffer.alloc(0)

  const publish = (id: string): boolean =>
    ch.publish("", q.queue, emptyBuffer, {
      correlationId: id,
      persistent: true,
    })

  const decode: Decode<string> = (msg) => msg.properties.correlationId

  return {
    publish,
    consume: createConsume(ch, q.queue, decode),
  }
}

export const ClearTimeoutExchange = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const e = await ch.assertExchange(config.prefix + ".timeout.clear", "fanout")

  const publish = (id: string): boolean =>
    ch.publish(e.exchange, "", Buffer.from(id, "utf8"), { persistent: true })

  const decode: Decode<string> = (msg) => msg.content.toString("utf8")

  const consume: Consume<string> = async (onMessage) => {
    const q = await ch.assertQueue("", { exclusive: true })
    await ch.bindQueue(q.queue, e.exchange, "")
    return createConsume(ch, q.queue, decode)(onMessage)
  }

  return {
    publish,
    consume,
  }
}

export const ProviderQueue = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const prefix = config.prefix + ".providers."

  const publish = (name: string, message: ProviderMessage): boolean =>
    ch.publish("", prefix + name, jsonBuf(message), { persistent: true })

  const decode: Decode<ProviderMessage> = (msg) => parseJsonBuf(msg.content)

  const consume = async (
    name: string,
    onMessage: (msg: ProviderMessage) => Promise<void>
  ) => {
    const q = await ch.assertQueue(prefix + name)
    return createConsume(ch, q.queue, decode)(onMessage)
  }

  return {
    publish,
    consume,
  }
}

export const SentResultQueue = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const q = await ch.assertQueue(config.prefix + ".router.sent")

  const publish = (results: Array<SentResult>): boolean =>
    ch.publish("", q.queue, jsonBuf(results), { persistent: true })

  const decode: Decode<Array<SentResult>> = (msg) => parseJsonBuf(msg.content)

  return {
    publish,
    consume: createConsume(ch, q.queue, decode),
  }
}

export const DeliveredQueue = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const q = await ch.assertQueue(config.prefix + ".router.delivered")

  const publish = (delivered: Array<Delivered>): boolean =>
    ch.publish("", q.queue, jsonBuf(delivered), { persistent: true })

  const decode: Decode<Array<Delivered>> = (msg) => parseJsonBuf(msg.content)

  return {
    publish,
    consume: createConsume(ch, q.queue, decode),
  }
}

export const FailedExchange = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const e = await ch.assertExchange(config.prefix + ".router.failed", "fanout")

  const publish = (failed: Failed): boolean =>
    ch.publish(e.exchange, "", jsonBuf(failed), { persistent: true })

  const decode: Decode<Failed> = (msg) => parseJsonBuf(msg.content)

  const consume: Consume<Failed> = async (onMessage) => {
    const q = await ch.assertQueue("", { exclusive: true })
    await ch.bindQueue(q.queue, e.exchange, "")
    return createConsume(ch, q.queue, decode)(onMessage)
  }

  return {
    publish,
    consume,
  }
}

export const CompletedExchange = async (di: DI) => {
  const config = di(Config)
  const ch = await di(Channel)
  const e = await ch.assertExchange(
    config.prefix + ".router.completed",
    "fanout"
  )

  const publish = (completed: Completed): boolean =>
    ch.publish(e.exchange, "", jsonBuf(completed), { persistent: true })

  const decode: Decode<Completed> = (msg) => parseJsonBuf(msg.content)

  const consume: Consume<Completed> = async (onMessage) => {
    const q = await ch.assertQueue("", { exclusive: true })
    await ch.bindQueue(q.queue, e.exchange, "")
    return createConsume(ch, q.queue, decode)(onMessage)
  }

  return {
    publish,
    consume,
  }
}
