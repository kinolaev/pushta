import http from "http"
import { DI } from "../../di"
import { Config } from "../../services/Config"
import { readJsonBufferStream } from "../../utils"

const TestMessage = (di: DI) => {
  const config = di(Config)

  const req = http.request(
    "http://localhost:" + config.sources.http.port,
    { method: "POST" },
    (res) => {
      readJsonBufferStream(res).then(console.log)
    }
  )
  req.on("error", console.error)
  req.end(
    JSON.stringify({
      text: "hello",
      tags: ["user:1"],
      data: [{ providers: ["log"], data: { title: "title" } }],
      tasks: [
        {
          targets: [
            {
              provider: "fail",
              address: "john-phone-token",
            },
            {
              provider: "log",
              address: "john-tablet-token",
            },
          ],
          fallback: [
            {
              targets: [
                {
                  provider: "never",
                  address: "john-phone-number",
                  options: {},
                },
              ],
            },
          ],
        },
        {
          options: [{ data: { wait: 10000 } }],
          ttl: 5,
          targets: [
            {
              provider: "fail",
              address: "maria-phone-token",
            },
          ],
          fallback: [
            {
              options: [{ data: { skipDelivered: true } }],
              targets: [
                {
                  provider: "log",
                  address: "maria-phone-number",
                },
              ],
            },
          ],
        },
      ],
    })
  )
}

if (require.main === module) {
  TestMessage(DI())
}
