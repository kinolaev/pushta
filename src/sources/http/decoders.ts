import { a1, new_ } from "elow"
import * as D from "elow/lib/Json/Decode"
import { MessageInput, TaskInput, TargetInput, ProvidersData } from "../../data"
import { opField } from "../../decoders"

export const providersData = D.map(
  a1(new_, ProvidersData),
  opField("providers", D.list(D.string), null),
  D.field("data", D.value)
)

export const targetInput = D.map(
  a1(new_, TargetInput),
  D.field("provider", D.string),
  D.field("address", D.string),
  opField("ttl", D.number, null),
  opField("data", D.value, null),
  opField("options", D.value, null)
)

export const taskInput = D.map(
  a1(new_, TaskInput),
  opField("ttl", D.number, null),
  D.field("targets", D.list(targetInput)),
  opField("options", D.list(providersData), null),
  opField(
    "fallback",
    D.list(D.lazy((): D.Decoder<TaskInput> => taskInput)),
    null
  )
)

export const messageInput = D.map(
  a1(new_, MessageInput),
  D.field("text", D.string),
  opField("ttl", D.number, null),
  opField("tags", D.list(D.string), null),
  opField("data", D.list(providersData), null),
  opField("options", D.list(providersData), null),
  D.field("tasks", D.list(taskInput))
)
