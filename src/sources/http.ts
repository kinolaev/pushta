import http from "http"
import { DI } from "../di"
import { MessagesQueue } from "../services/AMQP"
import { Config } from "../services/Config"
import { Store } from "../services/Store"
import { decodeString } from "../decoders"
import { readStringBufferStream, unknownErrorMessage, sendJson } from "../utils"
import { messageInput as messageInputDecoder } from "./http/decoders"

export const HttpSource = async (di: DI) => {
  const config = di(Config).sources.http
  const store = di(Store)
  const messagesQueue = await di(MessagesQueue)

  const srv = http.createServer((req, res) => {
    const userAgent = req.headers["user-agent"]
    if (typeof userAgent === "string" && userAgent.startsWith("kube-probe/"))
      return res.end()

    if (req.method === "POST") {
      readStringBufferStream(req)
        .then(async (body) => {
          const messageInput = decodeString(messageInputDecoder, body)
          if (messageInput instanceof Error) {
            const errorJson = JSON.stringify(messageInput.message)
            console.error("sendJson status=400 error=" + errorJson)
            return sendJson(res, 400, { error: messageInput.message })
          }

          console.log("addMessage tags=" + JSON.stringify(messageInput.tags))
          const id = await store.addMessage(messageInput)

          messagesQueue.publish(id)

          console.log("sendJson status=200 id=" + id)
          return sendJson(res, 200, { id })
        })
        .catch((error) => {
          const message = unknownErrorMessage(error)
          console.error("sendJson status=500 error=" + JSON.stringify(message))
          return sendJson(res, 500, { error: message })
        })
    } else {
      res.writeHead(404)
      res.end()
    }
  })
  srv.listen(config.port)
}

if (require.main === module) {
  HttpSource(DI())
}
