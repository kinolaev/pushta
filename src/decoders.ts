import { match as matchResult } from "elow/lib/Result"
import * as D from "elow/lib/Json/Decode"

export const decodeString = <A>(
  decoder: D.Decoder<A>,
  json: string
): Error | A =>
  matchResult(D.decodeString(json, decoder), {
    Err(error) {
      return new Error(D.decodeErrorToString(error))
    },
    Ok(value): Error | A {
      return value
    },
  })

export const opField = <A, B>(
  key: string,
  decoder: D.Decoder<A>,
  defaultValue: B
): D.Decoder<A | B> =>
  D.andThen(
    (value): D.Decoder<A | B> =>
      value === undefined ? D.succeed(defaultValue) : D.field(key, decoder),
    D.oneOf(D.field(key, D.value), D.succeed(undefined))
  )
