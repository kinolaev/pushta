export type DI = <T>(provider: Provider<T>) => T
export type Provider<T> = (di: DI) => T
export type Provided<P> = P extends Provider<infer T> ? Awaited<T> : P
export type Awaited<T> = T extends PromiseLike<infer U> ? Awaited<U> : T

type ServiceMap = Map<Provider<any>, any>

export type DIEntry<T> = [Provider<T>, Provider<T>]
export const DIEntry = <T>(i: Provider<T>, o: Provider<T>): DIEntry<T> => [i, o]

export const DI = (...entries: Array<DIEntry<any>>): DI => {
  const providers = new Map(entries)
  const services: ServiceMap = new Map()
  const di: DI = (provider) =>
    getOrCreate(di, services, providers.get(provider) ?? provider)
  return di
}

const getOrCreate = <T>(di: DI, sm: ServiceMap, p: Provider<T>): T =>
  sm.get(p) ?? set(sm, p, p(di))
const set = <K, V>(map: Map<K, V>, key: K, value: V): V => {
  map.set(key, value)
  return value
}
