import type { ServerResponse } from "http"
import type { Readable } from "stream"
import crypto from "crypto"
import http from "http"
import https from "https"
import ProxyAgent from "proxy-agent"

export const readStream = <T, R>(
  stream: Readable,
  callbackfn: (
    previousValue: R,
    currentValue: T,
    currentIndex: number,
    stream: Readable
  ) => R,
  initialValue: R
): Promise<R> =>
  new Promise((resolve, reject) => {
    let index = 0
    let result = initialValue
    stream.on("error", reject)
    stream.on("data", (chunk) => {
      result = callbackfn(result, chunk, index++, stream)
    })
    stream.on("end", () => {
      resolve(result)
    })
  })

export const readChunksStream = <T>(stream: Readable): Promise<Array<T>> =>
  readStream<T, Array<T>>(stream, addChunk, [])
const addChunk = <T>(chunks: Array<T>, chunk: T): Array<T> => {
  chunks.push(chunk)
  return chunks
}

export const readBufferStream = (stream: Readable): Promise<Buffer> =>
  readChunksStream<Buffer>(stream).then(Buffer.concat)
export const readStringBufferStream = (
  stream: Readable,
  encoding?: BufferEncoding
): Promise<string> =>
  readBufferStream(stream).then((buf) => buf.toString(encoding))
export const readJsonBufferStream = (stream: Readable): Promise<unknown> =>
  readStringBufferStream(stream).then(JSON.parse)

export const sleep = (ms: number): Promise<void> =>
  new Promise((resolve) => setTimeout(resolve, ms))

export const randomId = (n: number): string =>
  crypto.randomBytes(n).toString("hex")

export const jsonBuf = (value: unknown) =>
  Buffer.from(JSON.stringify(value), "utf8")

export const parseJsonBuf = <T>(buf: Buffer): Error | T =>
  try_(JSON.parse, buf.toString("utf8"))

export const minPositiveOrNull = (
  ...args: Array<number | null>
): number | null => {
  const nums = args.filter((arg) => arg !== null) as Array<number>
  if (nums.length === 0) return null
  const value = Math.min(...nums)
  return isNaN(value) || value <= 0 ? null : value
}

export const timestamp = (value: Date | number): number =>
  Math.floor((value instanceof Date ? value.getTime() : value) / 1000)

const base64Url = (buf: string | Buffer): string =>
  (buf instanceof Buffer ? buf : Buffer.from(buf, "utf8"))
    .toString("base64")
    .replace(/=/g, "")
    .replace(/\+/g, "-")
    .replace(/\//g, "_")

const jwtHeader = base64Url('{"alg":"HS256","typ":"JWT"}')

export const jwtSignature = (body: string, secret: string): string =>
  base64Url(
    crypto
      .createHmac("sha256", secret)
      .update([jwtHeader, base64Url(body)].join("."))
      .digest()
  )

export const unknownErrorMessage = (
  error: unknown,
  defaultMessage: string = "unknown"
): string =>
  error instanceof Error
    ? error.message
    : typeof error === "string"
    ? error
    : defaultMessage

export const try_ = <A extends ReadonlyArray<unknown>, R>(
  f: (...args: A) => R,
  ...args: A
): Error | R => {
  try {
    return f(...args)
  } catch (e: unknown) {
    return e instanceof Error
      ? e
      : e === undefined || typeof e === "string"
      ? new Error(e)
      : new Error(String(e))
  }
}

export const sendJson = (
  res: ServerResponse,
  statusCode: number,
  value: unknown
): Promise<void> =>
  new Promise((resolve) => {
    const body = JSON.stringify(value)
    res.writeHead(statusCode, {
      "content-type": "application/json",
      "content-length": Buffer.byteLength(body),
    })
    res.end(body, resolve)
  })

export const request = async (
  url: string | URL,
  opts: https.RequestOptions,
  body?: any
): Promise<http.IncomingMessage> => {
  const urlObj = url instanceof URL ? url : new URL(url)
  const mod = urlObj.protocol === "http:" ? http : https
  const agent =
    opts.agent !== undefined
      ? null
      : process.env.http_proxy ||
        process.env.https_proxy ||
        process.env.HTTP_PROXY ||
        process.env.HTTPS_PROXY
      ? new ProxyAgent()
      : null
  const reqOpts = agent === null ? opts : { agent, ...opts }
  return new Promise((resolve, reject) => {
    const req = mod.request(urlObj, reqOpts, resolve)
    req.on("error", reject)
    req.end(body)
  })
}
