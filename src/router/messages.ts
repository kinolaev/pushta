import { DI } from "../di"
import { TaskService } from "../services/Task"
import { MessagesQueue } from "../services/AMQP"

export const Messages = async (di: DI) => {
  const messagesQueue = await di(MessagesQueue)
  const taskService = await di(TaskService)

  messagesQueue.consume(taskService.startMessage)
}

if (require.main === module) {
  Messages(DI())
}
