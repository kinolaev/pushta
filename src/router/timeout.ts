import { DI } from "../di"
import { TaskService } from "../services/Task"
import { TimeoutQueue } from "../services/AMQP"

export const Timeout = async (di: DI) => {
  const timeoutQueue = await di(TimeoutQueue)
  const taskService = await di(TaskService)

  timeoutQueue.consume(async (id) => {
    const [messageId, taskId, targetId] = taskService.parseTimeoutId(id)

    if (targetId !== "")
      return taskService.targetFailed(messageId, taskId, targetId, "timeout")

    if (taskId !== "") return taskService.taskFailed(messageId, taskId)

    if (messageId !== "") return taskService.messageTimeout(messageId)
  })
}

if (require.main === module) {
  Timeout(DI())
}
