import { DI } from "../di"
import { Messages } from "./messages"
import { Timeout } from "./timeout"
import { Sent } from "./sent"
import { Delivered } from "./delivered"

export const Router = async (di: DI) => {
  await di(Messages)
  await di(Timeout)
  await di(Sent)
  await di(Delivered)
}

if (require.main === module) {
  Router(DI())
}
