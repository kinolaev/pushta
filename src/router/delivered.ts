import { DI } from "../di"
import { Store } from "../services/Store"
import { TaskService, parseTargetId } from "../services/Task"
import { DeliveredQueue } from "../services/AMQP"

export const Delivered = async (di: DI) => {
  const store = di(Store)
  const deliveredQueue = await di(DeliveredQueue)
  const taskService = await di(TaskService)

  deliveredQueue.consume(async (results) => {
    const now = Date.now()

    for (const delivered of results) {
      if (delivered.id !== undefined) {
        const [messageId, taskId, id] = parseTargetId(delivered.id)
        await taskService.targetDelivered(messageId, taskId, id, now)
        continue
      }

      const targets = await store.getTargetByExtId(
        delivered.provider,
        delivered.extId
      )

      if (targets.length === 0) {
        await store.addProviderDelivered({
          provider: delivered.provider,
          id: delivered.extId,
          created_at: now,
        })
        continue
      }

      for (const target of targets) {
        await taskService.targetDelivered(
          target.message_id,
          target.task_id,
          target.id,
          now
        )
      }
    }
  })
}

if (require.main === module) {
  Delivered(DI())
}
