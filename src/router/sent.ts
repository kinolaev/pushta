import { DI } from "../di"
import { Store } from "../services/Store"
import { TaskService, parseTargetId } from "../services/Task"
import { SentResultQueue, FailedExchange } from "../services/AMQP"

export const Sent = async (di: DI) => {
  const store = di(Store)
  const sentResultQueue = await di(SentResultQueue)
  const failedExchange = await di(FailedExchange)
  const taskService = await di(TaskService)

  sentResultQueue.consume(async (results) => {
    const delivereds = []

    for (const result of results) {
      const [messageId, taskId, id] = parseTargetId(result.id)
      const target = await store.getTarget(messageId, taskId, id)
      if (target === undefined) continue

      if (result.error !== undefined) {
        await taskService.targetFailed(messageId, taskId, id, result.error)
        failedExchange.publish({
          id: messageId,
          taskId,
          targetId: id,
          provider: target.provider,
          address: target.address,
          error: result.error,
        })
        continue
      }

      if (result.extId === undefined) continue

      await store.setTargetExtId(target, result.extId)

      const delivered = await store.getProviderDelivered(
        target.provider,
        result.extId
      )
      if (delivered === undefined) continue

      await taskService.targetDelivered(
        messageId,
        taskId,
        id,
        delivered.created_at
      )
      delivereds.push(delivered)
    }

    for (const delivered of delivereds) {
      await store.removeProviderDelivered(delivered.provider, delivered.id)
    }
  })
}

if (require.main === module) {
  Sent(DI())
}
