import http from "http"
import { DI } from "../di"
import { DeliveredQueue } from "../services/AMQP"
import { Config } from "../services/Config"
import { Store } from "../services/Store"
import {
  getProviderData,
  getTargetToken,
  parseTargetId,
} from "../services/Task"
import { try_ } from "../utils"

export const HttpDelivered = async (di: DI) => {
  const config = di(Config)
  const store = di(Store)
  const deliveredQueue = await di(DeliveredQueue)

  const srv = http.createServer((req, res) => {
    const userAgent = req.headers["user-agent"]
    if (typeof userAgent === "string" && userAgent.startsWith("kube-probe/"))
      return res.end()

    if (req.method === "GET") {
      const reqUrl = try_(() => new URL(req.url ?? "/", "http://localhost"))
      if (reqUrl instanceof Error) {
        res.writeHead(400, reqUrl.message)
        res.end()
        return
      }
      const targetId = reqUrl.pathname.substring(
        reqUrl.pathname.lastIndexOf("/") + 1
      )
      const [messageId, taskId, id] = parseTargetId(targetId)
      if (messageId === "" || taskId === "" || id === "") {
        res.writeHead(400)
        res.end()
        return
      }
      const token = reqUrl.searchParams.get("token")
      if (token === null) {
        res.writeHead(401)
        res.end()
        return
      }

      store
        .getTarget(messageId, taskId, id)
        .then(async (target) => {
          if (target === undefined) {
            res.writeHead(404)
            res.end()
            return
          }

          if (token !== getTargetToken(target, config.secret)) {
            res.writeHead(401)
            res.end()
            return
          }

          deliveredQueue.publish([{ id: targetId }])

          if (reqUrl.searchParams.has("include")) {
            const msg = await store.getMessage(messageId)
            if (msg === undefined) {
              res.writeHead(404)
              res.end()
              return
            }
            const task = await store.getTask(messageId, taskId)
            if (task === undefined) {
              res.writeHead(404)
              res.end()
              return
            }

            const data = getProviderData(target.provider, msg.data ?? [])
            if (target.data) data.push(target.data)

            const body = JSON.stringify({
              text: msg.text,
              data,
            })
            res.writeHead(200, {
              "content-type": "application/json",
              "content-length": Buffer.byteLength(body),
            })
            res.end(body)
          } else {
            res.end()
          }
        })
        .catch((err) => {
          console.error(err)
          res.writeHead(500)
          res.end()
        })
    } else {
      res.writeHead(404)
      res.end()
      return
    }
  })
  srv.listen(config.delivered.http.port)
}

if (require.main === module) {
  HttpDelivered(DI())
}
