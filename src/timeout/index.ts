import { DI } from "../di"
import {
  ClearTimeoutExchange,
  SetTimeoutQueue,
  TimeoutQueue,
} from "../services/AMQP"

export const Timeout = async (di: DI) => {
  const setTimeoutQueue = await di(SetTimeoutQueue)
  const timeoutQueue = await di(TimeoutQueue)
  const clearTimeoutExchange = await di(ClearTimeoutExchange)

  const timeouts = {} as { [id: string]: NodeJS.Timeout }

  setTimeoutQueue.consume(async ({ id, expire }) => {
    const now = Date.now()
    const exp = Date.parse(expire)

    if (isNaN(exp) || exp < now) {
      timeoutQueue.publish(id)
      return
    }

    const timeout = setTimeout(() => {
      timeoutQueue.publish(id)
      if (timeouts.hasOwnProperty(id)) {
        delete timeouts[id]
      }
    }, exp - now)

    if (timeouts.hasOwnProperty(id)) {
      clearTimeout(timeouts[id])
    }
    timeouts[id] = timeout
  })

  clearTimeoutExchange.consume(async (id) => {
    if (id.endsWith("*")) {
      const prefix = id.substring(0, id.length - 1)
      for (const timeoutId of Object.keys(timeouts)) {
        if (!timeoutId.startsWith(prefix)) continue
        clearTimeout(timeouts[timeoutId])
        delete timeouts[timeoutId]
      }
    } else {
      const timeoutId = id
      if (timeouts.hasOwnProperty(timeoutId)) {
        clearTimeout(timeouts[timeoutId])
        delete timeouts[timeoutId]
      }
    }
  })
}

if (require.main === module) {
  Timeout(DI())
}
