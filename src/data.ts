export class MessageInput {
  constructor(
    public text: string,
    public ttl: null | number,
    public tags: null | Array<string>,
    public data: null | Array<ProvidersData>,
    public options: null | Array<ProvidersData>,
    public tasks: Array<TaskInput>
  ) {}
}
export class TaskInput {
  constructor(
    public ttl: null | number,
    public targets: Array<TargetInput>,
    public options: null | Array<ProvidersData>,
    public fallback: null | Array<TaskInput>
  ) {}
}
export class TargetInput {
  constructor(
    public provider: string,
    public address: string,
    public ttl: null | number,
    public data: null | unknown,
    public options: null | unknown
  ) {}
}
export class ProvidersData {
  constructor(public providers: null | Array<string>, public data: unknown) {}
}

export type MessageResult = { error: string } | { id: string }

export interface Message {
  id: string
  created_at: number
  text: string
  ttl: null | number
  data: null | Array<ProvidersData>
  options: null | Array<ProvidersData>
}
export interface Task {
  message_id: string
  id: string
  depth: number
  started_at: null | number
  failed_at: null | number
  succeed_at: null | number
  ttl: null | number
  options: null | Array<ProvidersData>
}
export interface Target {
  message_id: string
  task_id: string
  id: string
  provider: string
  address: string
  ttl: null | number
  data: unknown
  options: unknown
  ext_id: string | null
  error: string | null
  delivered_at: number | null
}

export interface ProviderMessage {
  text: string
  targets: Array<ProviderTarget>
  data: Array<unknown>
  options: Array<unknown>
}
export interface ProviderTarget {
  id: string
  token: string
  address: string
  exp: number | null
  data: unknown
  options: unknown
}
export interface ProviderDelivered {
  provider: string
  id: string
  created_at: number
}

export type SentResult =
  | { id: string; error: string }
  | { id: string; error?: undefined; extId?: string }

export type Delivered =
  | { id: string; provider?: undefined; extId?: undefined }
  | { id?: undefined; provider: string; extId: string }

export interface Failed {
  id: string
  taskId: string
  targetId: string
  provider: string
  address: string
  error: string
}

export interface Completed {
  id: string
  failureCount: number
  successCount: number
}
