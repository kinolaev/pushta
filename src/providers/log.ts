import { DI } from "../di"
import {
  DeliveredQueue,
  ProviderQueue,
  SentResultQueue,
} from "../services/AMQP"
import { sleep, randomId } from "../utils"

export const LogProvider = async (di: DI) => {
  const sentResultQueue = await di(SentResultQueue)
  const deliveredQueue = await di(DeliveredQueue)
  const providerQueue = await di(ProviderQueue)

  providerQueue.consume("log", (msg) => {
    const promises = msg.targets.map(async (t, i) => {
      await sleep(1000)

      const extId = randomId(4)
      sentResultQueue.publish([{ id: t.id, extId }])

      await sleep(1000)

      console.log("log", t.id, t.token, extId, msg.text, t.address)
      return i % 2 === 0 ? { id: t.id } : { provider: "log", extId }
    })

    const opts = Object.assign({}, ...msg.options)
    if (opts.skipDelivered) {
      return Promise.resolve()
    }
    return Promise.all(promises).then((delivered) => {
      deliveredQueue.publish(delivered)
    })
  })
}

if (require.main === module) {
  LogProvider(DI())
}
