import apn from "@parse/node-apn"
import { DI } from "../di"
import { ProviderQueue, SentResultQueue } from "../services/AMQP"
import { Config } from "../services/Config"
import { unknownErrorMessage } from "../utils"

export const APNProvider = async (di: DI) => {
  const config = di(Config).providers.apn
  const providerQueue = await di(ProviderQueue)
  const sentResultQueue = await di(SentResultQueue)

  const provider = new apn.Provider({
    token: config.token,
    production: config.production,
    proxy: proxyFromEnv(),
  })

  providerQueue.consume("apn", async (msg) => {
    const msgData = Object.assign({}, ...msg.data, ...msg.options)
    msg.targets.forEach((target) => {
      const data = Object.assign({}, msgData, target.data, target.options)

      const note = new apn.Notification()

      note.pushType = data.pushType ?? "alert"
      note.topic = config.bundleId
      if (target.exp !== null) note.expiry = target.exp
      if (typeof data.priority === "number") note.priority = data.priority
      if (typeof data.collapseId === "string") note.collapseId = data.collapseId

      if (typeof data.aps === "object" && data.aps !== null) {
        note.aps = data.aps
      }

      if (typeof note.aps.alert === "object" && note.aps.alert !== null) {
        if (typeof note.aps.alert.title !== "string") {
          note.aps.alert.title = data.title
        }
        if (typeof note.aps.alert.body !== "string") {
          note.aps.alert.body = msg.text
        }
      } else if (typeof note.aps.alert !== "string") {
        note.aps.alert = { title: data.title, body: msg.text }
      }

      note.payload = Object.assign(
        { id: target.id, token: target.token },
        data.payload
      )

      provider
        .send(note, target.address)
        .then(({ failed, sent }) => {
          if (failed.length === 1) {
            const f = failed[0]
            const error =
              f.response?.reason ??
              f.error?.message ??
              (f.status ? "Status " + f.status : "unknown")
            sentResultQueue.publish([{ id: target.id, error }])
          } else if (sent.length === 1) {
            sentResultQueue.publish([{ id: target.id }])
          }
        })
        .catch((error) => {
          sentResultQueue.publish([
            { id: target.id, error: unknownErrorMessage(error) },
          ])
        })
    })
  })
}

const proxyFromEnv = (): { host: string; port: number } | undefined => {
  const httpProxy = process.env.http_proxy ?? process.env.HTTP_PROXY
  if (httpProxy === undefined) return
  const httpProxyUrl = new URL(httpProxy)
  return { host: httpProxyUrl.hostname, port: +httpProxyUrl.port || 80 }
}

if (require.main === module) {
  APNProvider(DI())
}
