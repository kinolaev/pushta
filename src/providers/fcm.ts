import path from "path"
import fs from "fs/promises"
import * as admin from "firebase-admin"
import ProxyAgent from "proxy-agent"
import { DI } from "../di"
import { ProviderQueue, SentResultQueue } from "../services/AMQP"
import { Config } from "../services/Config"
import { unknownErrorMessage } from "../utils"

export const FCMProvider = async (di: DI) => {
  const config = di(Config).providers.fcm
  const providerQueue = await di(ProviderQueue)
  const sentResultQueue = await di(SentResultQueue)

  if (process.env.GOOGLE_APPLICATION_CREDENTIALS === undefined) {
    if (config.credentials !== "") {
      const credPath = path.join(__dirname, "fcm-service-account.json")
      await fs.writeFile(credPath, config.credentials)
      process.env.GOOGLE_APPLICATION_CREDENTIALS = credPath
      config.credentials = ""
    }
  }

  const httpAgent = process.env.http_proxy
    ? new ProxyAgent(process.env.http_proxy)
    : process.env.HTTP_PROXY
    ? new ProxyAgent(process.env.HTTP_PROXY)
    : undefined

  admin.initializeApp({
    credential: admin.credential.applicationDefault(httpAgent),
    httpAgent,
  })

  providerQueue.consume("fcm", async (msg) => {
    const msgData = Object.assign({}, ...msg.data, ...msg.options)
    const messages = msg.targets.map((target) => {
      const data = Object.assign({}, msgData, target.data, target.options)

      const notificationData: Record<string, string> = { body: msg.text }
      if (typeof data.title === "string") {
        notificationData.title = data.title
        data.title = undefined
      }

      const hasNotification =
        typeof data.notification === "object" && data.notification !== null
      const hasData = typeof data.data === "object" && data.data !== null

      if (hasNotification) {
        data.notification = Object.assign(notificationData, data.notification)
      }
      if (hasData) {
        const targetData = { id: target.id, token: target.token }
        if (hasNotification) {
          data.data = Object.assign(targetData, data.data)
        } else {
          data.data = Object.assign(targetData, notificationData, data.data)
        }
      }
      if (!hasNotification && !hasData) {
        data.notification = notificationData
      }
      if (target.exp !== null) {
        data.android = Object.assign(
          { ttl: target.exp * 1000 - Date.now() },
          data.android
        )
      }
      if (typeof data.targetType === "string") {
        data[data.targetType] = target.address
        data.targetType = undefined
      } else {
        data.token = target.address
      }

      return data
    })
    admin
      .messaging()
      .sendAll(messages)
      .then(({ responses }) => {
        sentResultQueue.publish(
          responses.map((r, i) =>
            r.success
              ? { id: msg.targets[i].id, extId: r.messageId }
              : { id: msg.targets[i].id, error: r.error?.code }
          )
        )
      })
      .catch((error) => {
        sentResultQueue.publish(
          msg.targets.map((t) => ({
            id: t.id,
            error: unknownErrorMessage(error),
          }))
        )
      })
  })
}

if (require.main === module) {
  FCMProvider(DI())
}
