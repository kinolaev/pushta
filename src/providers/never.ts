import { DI } from "../di"
import { ProviderQueue } from "../services/AMQP"

export const NeverProvider = async (di: DI) => {
  const providerQueue = await di(ProviderQueue)

  providerQueue.consume("never", async (msg) => {
    console.error("never")
  })
}

if (require.main === module) {
  NeverProvider(DI())
}
