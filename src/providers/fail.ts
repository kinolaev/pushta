import { DI } from "../di"
import { ProviderQueue, SentResultQueue } from "../services/AMQP"
import { sleep } from "../utils"

export const FailProvider = async (di: DI) => {
  const providerQueue = await di(ProviderQueue)
  const sentResultQueue = await di(SentResultQueue)

  providerQueue.consume("fail", async (msg) => {
    const opts = Object.assign({}, ...msg.options)
    await sleep(opts.wait ?? 1000)
    const results = msg.targets.map((t) => ({
      id: t.id,
      error: "fail",
    }))
    sentResultQueue.publish(results)
  })
}

if (require.main === module) {
  FailProvider(DI())
}
