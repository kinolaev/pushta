import type { ProviderTarget } from "../data"
import xml from "fast-xml-parser"
import { DI } from "../di"
import { ProviderQueue, SentResultQueue } from "../services/AMQP"
import { Config } from "../services/Config"
import { request, readStringBufferStream } from "../utils"

export const SmsTrafficProvider = async (di: DI) => {
  const { url, login, password, rus } = di(Config).providers.smstraffic
  const providerQueue = await di(ProviderQueue)
  const sentResultQueue = await di(SentResultQueue)

  providerQueue.consume("smstraffic", async (msg) => {
    const phones = msg.targets.map(phoneFromProviderTarget).join(",")
    const params = {
      login,
      password,
      phones,
      message: msg.text,
      rus,
    }
    const reqBody = new URLSearchParams(params).toString()
    const headers = {
      "content-type": "application/x-www-form-urlencoded",
      "content-length": Buffer.byteLength(reqBody),
    }
    const res = await request(url, { method: "POST", headers }, reqBody).catch(
      (error: Error) => error
    )
    if (res instanceof Error) {
      const error = `Request error: ${res.message}`
      const results = msg.targets.map((t) => ({ id: t.id, error }))
      sentResultQueue.publish(results)
      return
    }
    const resBody = await readStringBufferStream(res).catch(
      (error: Error) => error
    )
    if (resBody instanceof Error) {
      const error = `Response error: ${resBody.message}`
      const results = msg.targets.map((t) => ({ id: t.id, error }))
      sentResultQueue.publish(results)
      return
    }
    if (
      res.statusCode === 200 &&
      xml.validate(resBody) === true &&
      xml.parse(resBody)?.reply?.result?.toUpperCase() === "OK"
    ) {
      sentResultQueue.publish(msg.targets.map((t) => ({ id: t.id })))
    } else {
      const error = `Status: ${res.statusCode}, body: ${resBody}`
      const results = msg.targets.map((t) => ({ id: t.id, error }))
      sentResultQueue.publish(results)
    }
  })
}

function phoneFromProviderTarget({ address }: ProviderTarget): string {
  return address.length === 10
    ? "7" + address
    : address.startsWith("+")
    ? address.substring(1)
    : address
}

if (require.main === module) {
  SmsTrafficProvider(DI())
}
