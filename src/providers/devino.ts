import type { ProviderMessage, ProviderTarget, SentResult } from "../data"
import xml from "fast-xml-parser"
import { DI } from "../di"
import { ProviderQueue, SentResultQueue } from "../services/AMQP"
import { Config } from "../services/Config"
import { request, readStringBufferStream } from "../utils"

export const DevinoProvider = async (di: DI) => {
  const { url, login, password, sender } = di(Config).providers.devino
  const providerQueue = await di(ProviderQueue)
  const sentResultQueue = await di(SentResultQueue)

  providerQueue.consume("devino", async (msg) => {
    sentResultQueue.publish(await send(url, login, password, sender, msg))
  })
}

export async function send(
  url: string,
  login: string,
  password: string,
  sender: string,
  msg: ProviderMessage
): Promise<Array<SentResult>> {
  const reqBody = reqBodyFromProviderMessage(login, password, sender, msg)
  const headers = {
    "content-type": "application/xml",
    "content-length": Buffer.byteLength(reqBody),
  }
  const res = await request(url, { method: "POST", headers }, reqBody).catch(
    (error: Error) => error
  )
  if (res instanceof Error) {
    const error = `Request error: ${res.message}`
    return msg.targets.map((t) => ({ id: t.id, error }))
  }
  const resBody = await readStringBufferStream(res).catch(
    (error: Error) => error
  )
  if (resBody instanceof Error) {
    const error = `Response error: ${resBody.message}`
    return msg.targets.map((t) => ({ id: t.id, error }))
  }
  if (res.statusCode !== 200 && xml.validate(resBody) !== true) {
    const error = `Status: ${res.statusCode}, body: ${resBody}`
    return msg.targets.map((t) => ({ id: t.id, error }))
  }
  return sentResultsFromResBody(msg.targets, resBody)
}

function reqBodyFromProviderMessage(
  login: string,
  password: string,
  sender: string,
  msg: ProviderMessage
): string {
  const j2xParser = new xml.j2xParser({
    ignoreAttributes: false,
    attrNodeName: "$",
    supressEmptyNode: true,
  })
  const reqBody = j2xParser.parse({
    package: {
      $: { login, password },
      message: {
        default: { $: { sender } },
        msg: msg.targets.map((t) => ({
          $: { recipient: phoneFromProviderTarget(t), type: "0" },
          "#text": msg.text,
        })),
      },
    },
  })
  return '<?xml version="1.0" encoding="utf-8" ?>' + reqBody
}

function phoneFromProviderTarget({ address }: ProviderTarget): string {
  return address.length === 10
    ? "+7" + address
    : address.startsWith("+") === false
    ? "+" + address
    : address
}

export function sentResultsFromResBody(
  targets: Array<ProviderTarget>,
  body: string
): Array<SentResult> {
  const resObj = xml.parse(body, {
    ignoreAttributes: false,
    attributeNamePrefix: "",
    attrNodeName: "$",
    parseNodeValue: false,
  })
  const errorCode = resObj?.package?.error
  if (errorCode) {
    const error = `Package status: ${errorCode}`
    return targets.map((t) => ({ id: t.id, error }))
  }
  const msg = resObj?.package?.message?.msg
  const msgs = Array.isArray(msg) ? msg : msg === undefined ? [] : [msg]
  return targets.map((t, i) =>
    msgs[i]?.["#text"] === "106"
      ? { id: t.id, extId: msgs[i].$.sms_id }
      : { id: t.id, error: `Message status: ${msgs[i]?.["#text"]}` }
  )
}

if (require.main === module) {
  DevinoProvider(DI())
}
