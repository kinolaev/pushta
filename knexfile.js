"use strict"

const { DI } = require("./lib/di")
const { Config } = require("./lib/services/Config")

const config = Config(DI())
const knexStorePath = "services/Store/KnexStore"

module.exports = {
  client: "pg",
  connection: config.databaseUrl,
  migrations: process.argv.includes("migrate:make")
    ? { directory: `./src/${knexStorePath}/migrations`, extension: "ts" }
    : { directory: `./lib/${knexStorePath}/migrations` },
  seeds: process.argv.includes("seed:make")
    ? { directory: `./src/${knexStorePath}/seeds`, extension: "ts" }
    : { directory: `./lib/${knexStorePath}/seeds` },
}
