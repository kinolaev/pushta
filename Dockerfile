FROM node:14-alpine AS build

WORKDIR /app

COPY package*.json ./
RUN npm install

COPY . .
RUN sed -i 's/^type ResolveResult<S>/export type ResolveResult<S>/' node_modules/knex/types/index.d.ts && npm run build && npm prune --production


FROM node:14-alpine

WORKDIR /app

COPY --from=build /app/node_modules node_modules/
COPY --from=build /app/lib lib/
COPY --from=build /app/knexfile.js knexfile.js

CMD [ "node", "lib/index.js" ]
