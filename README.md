# Пушта

Сервис маршрутизации сообщений.
Позволяет отправлять сообщения на устройства с ОС Android, устройства Apple, а также добавлять иные способы доставки сообщений.

## Быстрый старт

1. Установите [Docker](https://docs.docker.com/get-docker/) и [Docker Compose](https://docs.docker.com/compose/install/)
1. Для отправки сообщение на устройства с ОС Android [получите реквизиты от Firebase Cloud Messaging (FCM)](https://firebase.google.com/docs/cloud-messaging/auth-server#provide-credentials-manually) и сохраните их в `config/fcm-service-account.json`
1. Для отправки сообщение на устройства Apple [получите ключ шифрования от Apple Push Notification service (APNs)](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/establishing_a_token-based_connection_to_apns), сохраните его в файл `config/apn-key.p8`, создайте файл `config/.env` и добавьте в него переменные:
   ```
   PUSHTA_PROVIDERS_APN_TOKEN_KEY_ID=Key ID, идентификатор полученного ключа
   PUSHTA_PROVIDERS_APN_TOKEN_TEAM_ID=Team ID из раздела Membership
   PUSHTA_PROVIDERS_APN_BUNDLE_ID=Bundle ID из раздела информации о приложении
   ```
1. Выполните в терминале
   ```bash
   docker-compose up
   ```
1. Создайте таблице в базе данных
   ```bash
   docker-compose exec pushta npx knex migrate:latest
   ```
1. Отправьте сообщение:
   ```bash
   curl http://localhost:5000 -d '{ "text":"привет!", "tasks": [{ "targets": [{ "provider": "fcm", "address": "токен устройства c Android" }, { "provider": "apn", "address": "токен устройства Apple" }] }] }'
   ```

Документация по получению токенов: [FCM](https://firebase.google.com/docs/cloud-messaging/android/client), [APNs](https://developer.apple.com/documentation/usernotifications/registering_your_app_with_apns).
Включить FCM API https://console.developers.google.com/apis/api/googlecloudmessaging.googleapis.com/overview

## Требования

- [RabbitMQ](https://www.rabbitmq.com) 3.8
- [PostgreSQL](https://www.postgresql.org) 13
- [Node.js](https://nodejs.org) 14
- cpu 2000m
- memory 2Gi

## Сетевой доступ

- https://fcm.googleapis.com для отправки сообщений на устройства с ОС Android
- https://api.sandbox.push.apple.com (тест) и https://api.push.apple.com (бой) для отправки сообщений на устройства Apple

## Параметры запуска

Пушта использует следующие переменные окружения:

- `PUSHTA_PREFIX` префикс для имен очередей в RabbitMQ (по умолчанию - `pushta`)
- `PUSHTA_SECRET` секретный ключ для создания токенов доступа к сообщениям (по умолчанию - `pushta`)
- `PUSHTA_AMQP_URL` адрес RabbitMQ
- `PUSHTA_DATABASE_URL` адрес PostgreSQL
- `PUSHTA_SOURCES_HTTP_PORT` порт для входчщих сообщений
- `PUSHTA_PROVIDERS_APN_TOKEN_KEY` ключ или путь к файлу ключа APNs
- `PUSHTA_PROVIDERS_APN_TOKEN_KEY_ID` Key ID ключа APNs
- `PUSHTA_PROVIDERS_APN_TOKEN_TEAM_ID` Team ID
- `PUSHTA_PROVIDERS_APN_PRODUCTION` признак использования боевой среды (по умолчанию `true`)
- `PUSHTA_PROVIDERS_APN_BUNDLE_ID` Bundle ID приложения
- `GOOGLE_APPLICATION_CREDENTIALS` путь к файлу реквизитов FCM
- `PUSHTA_PROVIDERS_FCM_CREDENTIALS` содержимое файла реквизитов FCM (необходимо только в случае отсутствия предыдущей переменной)
- `PUSHTA_DELIVERED_HTTP_PORT` порт для уведомлений о доставке

## Отправка сообщений

Отправка сообщения осуществляется по протоколу `http` методом `post` в формате `json`

### Структура

```ts
// Тело запроса создания сообщения
interface MessageInput {
  // текст сообщения
  text: string
  // время жизни сообщения в секундах, по истечению которого
  // попытки отправить сообщение будут прекращены
  ttl?: number
  // произвольный набор строк, например,
  // для поиска сообщений по клиенту
  tags?: Array<string>
  // данные для провайдеров
  data?: Array<ProvidersData>
  // параметры для провайдеров
  options?: Array<ProvidersData>
  // задания на отправку (основные и запасные варианты доставки)
  tasks: Array<TaskInput>
}
interface TaskInput {
  // время жизни задания в секундах, по истечению которого
  // оно будет считаться неуспешным и будут запушены задания из поля fallback
  ttl?: number
  // получатели сообщения
  targets: Array<TargetInput>
  // параметры для провайдеров
  options?: Array<ProvidersData>
  // задания на отправку, которые начнут выполняться,
  // если ни один из получателей поля targets не получит сообщение
  fallback?: Array<TaskInput>
}
interface TargetInput {
  // идентификатор провайдера, например, fcm
  provider: string
  // идентификатор получателя в рамках провайдера,
  // например, номер телефона для sms и токены для пушей
  address: string
  // время на доставку сообщения получателю
  ttl?: number
  // данные для провайдера, соответсвующего идентификатору в поле provider
  data?: Json
  // параметры для провайдера, соответсвующего идентификатору в поле provider
  options?: Json
}
interface ProvidersData {
  // список идентификаторов провайдеров, которым необходимо передать данные
  providers?: Array<string>
  // сами данные
  data: Json
}
type Json =
  | null
  | boolean
  | number
  | string
  | Array<Json>
  | { [key: string]: Json }
```

<details>
  <summary>Примеры</summary>
  
  #### Уведомления об операциях

PUSH на все устройства пользователся с таймаутом 30 секунд и SMS, если ни один PUSH не доставлен

```json
{
  "text": "привет",
  "tags": ["user:1"],
  "options": [
    {
      "providers": ["fcm", "apn"],
      "data": { "title": "заголовок" }
    }
  ],
  "tasks": [
    {
      "ttl": 30,
      "targets": [
        { "provider": "fcm", "address": "токен android телефона" },
        { "provider": "fcm", "address": "токен android планшета" },
        { "provider": "apn", "address": "токен ipad" }
      ],
      "fallback": [
        {
          "targets": [{ "provider": "sms", "address": "номер телефона" }]
        }
      ]
    }
  ]
}
```

#### Маркетинговая рассылка

PUSH списку получателей, для каждого из которых возможны свои запасные варианты доставки (fallback)

```json
{
  "text": "hello",
  "tasks": [
    {
      "targets": [
        {
          "provider": "fcm",
          "address": "токен android устройства пользователя 1"
        }
      ],
      "fallback": [
        {
          "targets": [
            { "provider": "sms", "address": "номер телефона пользователя 1" }
          ]
        }
      ]
    },
    {
      "targets": [
        { "provider": "apn", "address": "токен ios устройства пользователя 2" },
        { "provider": "apn", "address": "токен ios устройства пользователя 3" }
      ]
    }
  ]
}
```

В отличие от примера с уведомлением об операции (для которых доставка обязательна), в данном примере в первой задаче (`.tasks[0]`) свойство `ttl` не указано, поэтому SMS будет отправлено, только если `fcm` не примет сообщение (например, если токен устарел). Если `fcm` примет сообщение, то SMS не будет отправлено независимо от того, пришлёт ли приложение уведомление о получении.

</details>

### Ответ

```ts
type MessageResult =
  // в случае ошибки ответ содержит поле error, с описанием ошибки
  | { error: string }
  // в случае принятия сообщения к отправке ответ содержит
  // идентификатор этого сообщения в рамках сервиса
  | { id: string }
```

<details>
  <summary>Примеры</summary>

#### Сообщение принято

```json
{ "id": "1" }
```

#### Ошибка

```json
{ "error": ".text is undefined" }
```

</details>

## Данные и параметры для FCM

FCM принимает два типа сообщений: [Notification message и Data message](https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages).
Первые отображаются пользователю, вторые вызывают обработчик в приложении.
По умолчанию `fcm` провайдер отправляет `Notification message`.
Чтобы отправить `Data message` данные провайдеру `fcm` должны иметь поле `data`.
[Документация по параметрам провайдера](https://firebase.google.com/docs/reference/admin/node/admin.messaging.TokenMessage).
Если данные провайдеру содержат поле `data`, провайдер добавляет в него поля `id` и `token` (необходимы для подтверждения получения сообщения).
Поле `title` со значением поля `title` данных провайдера и поле `body` с текстом сообщения добавляются в поле `notification`, а в случае отсутствия последнего - в поле `data`.

<details>
  <summary>Примеры</summary>

При передаче в Пушту

```json
{ "text": "привет" }
```

в FCM будет передано `Notification message`

```json
{ "notification": { "body": "привет" } }
```

При передаче в Пушту

```json
{
  "text": "привет",
  "data": [
    { "providers": ["fcm"], "data": { "title": "заголовок", "data": {} } }
  ]
}
```

в FCM будет передано `Data message`

```json
{
  "data": {
    "id": "1",
    "token": "secret",
    "title": "заголовок",
    "body": "привет"
  }
}
```

При передаче в Пушту

```json
{
  "text": "привет",
  "data": [
    {
      "providers": ["fcm"],
      "data": { "title": "заголовок", "notification": {}, "data": {} }
    }
  ]
}
```

в FCM будет передано

```json
{
  "notification": { "title": "заголовок", "body": "привет" },
  "data": { "id": "1", "token": "secret" }
}
```

</details>

По умолчанию поле `address` получателя передается в поле `token`. Чтобы передать адрес в `topic` или `condition` необходимо в параметрах провайдеру указать соответствующий `targetType`.

## Данные и параметры для APN

Параметры `pushType`, `priority` и `collapseId` соответствуют заголовкам `apns-push-type`, `apns-priority` и `apns-collapse-id` из [документации Apple](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/sending_notification_requests_to_apns).
Данные могут содержать поля `payload` (данные для приложения) и [`aps`](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/generating_a_remote_notification).
Поля `id` и `token` (необходимы для подтверждения получения сообщения) добавляются в `payload`.
Поле `title` с значением поля `title` данных провайдера и поле `body` с текстом сообщения добавляются в поле `aps.alert`.

<details>
  <summary>Примеры</summary>

При передаче в Пушту

```json
{ "text": "привет" }
```

в APNs будет передано

```json
{ "aps": { "alert": { "body": "привет" } }, "id": "1", "token": "secret" }
```

При передаче в Пушту

```json
{
  "text": "привет",
  "data": [
    {
      "providers": ["apn"],
      "data": { "aps": { "sound": "default" }, "some": "value" }
    }
  ]
}
```

в APNs будет передано

```json
{
  "aps": { "alert": { "body": "привет" }, "sound": "default" },
  "some": "value",
  "id": "1",
  "token": "secret"
}
```

</details>

## Отправка уведомлений о получении

FCM и APNs не гарантируют доставку сообщения на устройство, но позволяют обработать полученные приложением данные перед показом уведомления пользователю.
В Android для этого необходимо отправить `Data message`, а в операционных системах Apple - реализовать [Service App Extension](https://developer.apple.com/documentation/usernotifications/modifying_content_in_newly_delivered_notifications).
Pushta передает идентификатор получателя сообщения и авторизационнный токен в полях `id` и `token`, соответственно.
Чтобы подтвердить получение сообщения, приложение должно отправить `http` запрос методом `GET` со значением`id` в пути запроса и значением `token` в параметре `token`.

```
GET /messages/{id}?token={token}&include HTTP/1.1
```

При наличии параметра `include` ответ будет содержать текст сообщения и данные провайдера

```ts
interface TargetMessage {
  // текст сообщения из MessageInput
  text: string
  // данные провайдера
  data: Array<Json>
}
```

Параметры провайдера возвращены не будут, в этом их отличие от данных.

## Получение уведомлений о завершении заданий сообщения

Уведомления о завершении заданий сообщения отправляются в `RabbitMQ` в `exchange` `${PUSHTA_PREFIX}.router.completed` типа `fanout`.
Уведомления отправляются, только когда все задания сообщения завершены (чтобы задания завершались, необходимо указывать `ttl`).
Уведомления содержат количество успешно и неуспешно завершенных заданий.
Учитываются только те задания, которые связаны с сообщением непосредственно, fallback-задания не учитываются.
Если задание завершено неуспешно, но хотя бы одно из fallback-заданий завершено успешно, задание считается завершенным успешно.

```ts
interface Completed {
  // идентификатор сообщения, полученный от сервиса
  id: string
  // количество неуспешных заданий
  failureCount: number
  // количество успешных заданий
  successCount: number
}
```

<details>
  <summary>Пример</summary>

```json
{ "id": "1", "failureCount": 1, "successCount": 1 }
```

</details>

## Архитектура

Пушта состоит из нескольких модулей:

- `router` распределяет сообщения между провайдерами
- `timeout` отслеживает время жизни сообщений
- `sources/http` принимает сообщения по протоколу `http`
- `providers/fcm` провайдер сообщений на устройства с ОС Android
- `providers/apn` провайдер сообщений на устройства Apple
- `delivered/http` принимает уведомления о получении сообщения от любых получателей по протоколу `http`

Каждый модуль может быть запущен как отдельное приложения в любом количестве экземпляров. Например, чтобы запустить еще один экземпляр `apn` провайдера, нужно выполнить `node lib/providers/apn`. Также возможно отключить любой из модулей с помощью соответствующей переременной среды:

```
PUSHTA_ROUTER_ENABLED=false
PUSHTA_TIMEOUT_ENABLED=false
PUSHTA_SOURCES_HTTP_ENABLED=false
PUSHTA_PROVIDERS_APN_ENABLED=false
PUSHTA_PROVIDERS_FCM_ENABLED=false
PUSHTA_DELIVERED_HTTP_ENABLED=false
```

## Создание собственного провайдера

Для реализации собственного провайдера достаточно подключиться к очереди `RabbitMQ` с именем `${PUSHTA_PREFIX}.providers.${идентификатор провайдера}`.
Роутер будет отправлять в эту очередь сообщения следующей структуры:

```ts
interface ProviderMessage {
  // текст сообщение из MessageInput
  text: string
  // получатели из TargetInput с соответсвующим
  // идентифкатором в поле provider
  targets: Array<ProviderTarget>
  // массив данных провайдера
  data: Array<Json>
  // массив параметров провайдера
  options: Array<Json>
}
interface ProviderTarget {
  // идентификатор получателя сервиса
  id: string
  // секретная строка для проверки уведомления о доставке
  token: string
  // идентификатор получателя из TargetInput
  address: string
  // данные провайдера, специфичные для получателя
  data: Json
  // параметры провайдера, специфичные для получателя
  options: Json
}
```

<details>
  <summary>Пример</summary>

```json
{
  "text": "привет",
  "data": [{ "title": "заголовок" }],
  "targets": [
    { "id": "1", "token": "secret1", "address": "токен устройства 1" },
    { "id": "2", "token": "secret2", "address": "токен устройства 2" }
  ]
}
```

</details>

Приняв сообщение, провайдер должен передать в очередь `${PUSHTA_PREFIX}.router.sent` ошибки, если такие возникли, или идентификаторы отправленных сообщений во внешней системе, если имеются

```ts
type SentResult =
  // в случае ошибки отправки ответ содержит
  // идентификатор из ProviderTarget и ошибку
  | { id: string; error: string }
  // в случае успеха - идентификатор из ProviderTarget
  // и идентификатор сообщения во внешней системе,
  // если сервис предоставляет такие идентификаторы
  | { id: string; extId?: string }
```

<details>
  <summary>Пример</summary>

```json
[
  { "id": "1", "error": "invalid address" },
  { "id": "2", "extId": "321" }
]
```

</details>

Опционально, провайдер может сообщить о доставке сообщения получателю, отправив сообщение в очередь `${PUSHTA_PREFIX}.router.delivered`, содержащее массив внутренних или внешних идентификаторов:

```ts
type Delivered =
  // идентифкатор из ProviderTarget
  | { id: string }
  // идентифкатор сообщения во внешней системе,
  // переданный ранее в SentResult в поле extId
  | { provider: string; extId: string }
```

<details>
  <summary>Пример</summary>

```json
[{ "id": "1" }, { "provider": "log", "extId": "321" }]
```

</details>

## Пример провайдера на nodejs

<details>
  <summary>Провайдер Log</summary>

```js
const amqp = require("amqplib")

// Подключаемся к RabbitMQ
amqp.connect("amqp://localhost").then(async (conn) => {
  // Создаем канал
  const ch = await conn.createChannel()
  // Создаем очередь сообщений, если она не была создана ранее
  const q = await ch.assertQueue("pushta.providers.log")
  // Подлючаемся к очереди
  ch.consume(q.queue, (msg) => {
    if (msg === null) return

    /** @type {ProviderMessage} */
    const message = JSON.parse(msg.content.toString("utf8"))

    /** @type {Array<SentResult>} */
    const sentResult = []

    for (const target of message.targets) {
      // Отправляем сообщение получателю
      const extId = fakeMessagingService(
        message.text,
        target.address,
        handleDelivered
      )
      // Сохраняем результат отправки
      sentResult.push({ id: target.id, extId: extId })
    }
    // Отправляем уведомление об отправке в очередь
    ch.sendToQueue(
      "pushta.router.sent",
      Buffer.from(JSON.stringify(sentResult), "utf8")
    )
    // Завершаем обработку сообщения
    ch.ack(msg)
  })

  // Обработчик уведомлений о доставке
  const handleDelivered = (id) => {
    /** @type {Array<Delivered>} */
    const delivered = [{ provider: "log", extId: id }]
    // Отправляем уведомление о доставке в очередь
    ch.sendToQueue(
      "pushta.router.delivered",
      Buffer.from(JSON.stringify(delivered), "utf8")
    )
  }
})

// Внешний сервис сообщений
const fakeMessagingService = (text, address, onDelivered) => {
  // (опционально) Генерирует внешний идентификатор
  const id = Math.round(Math.random() * 10000).toString()

  setTimeout(() => {
    // Доставляет сообщение
    console.log("log", id, text, address)
    // (опционально) Уведомляет о доставке
    onDelivered(id)
  }, 1000)

  return id
}
```

</details>

## Разработка

Среда разработки преднастроена для редактора [VSCode](https://code.visualstudio.com) с расширением [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers).
Также можно использовать:

```bash
docker-compose -f docker-compose.yaml -f .devcontainer/docker-compose.yml up
docker-compose exec pushta sh
```

После перехода в контейнер необходимо установить зависимости

```bash
npm install
```

Запустить сборку проекта

```bash
npx tsc -w
```

В новом терминале запустить приложение:

```bash
npx nodemon
```
